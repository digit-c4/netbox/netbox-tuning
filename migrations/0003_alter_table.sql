-- +goose NO TRANSACTION
-- +goose Up
ALTER TABLE netbox_kpi.TestCases ADD COLUMN IF NOT EXISTS RowsDump UInt32;

-- +goose Down

ALTER TABLE netbox_kpi.TestCases DROP COLUMN IF EXISTS RowsDump;
