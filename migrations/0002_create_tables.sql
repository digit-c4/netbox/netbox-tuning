-- +goose NO TRANSACTION
-- +goose Up

-- Create Test table
CREATE TABLE IF NOT EXISTS netbox_kpi.TestCases (
    Id String PRIMARY KEY,
    TestDate DateTime,
    NetboxDistributionVersion String,
    Cpu String,
    Ram Int32,
    RedisVersion String,
    PostgresqlVersion String,
    PostgresMaxConnections Int32,
    PostgresSharedBuffers Int32,
    TestContext String,
) ENGINE = MergeTree()
ORDER BY Id;

-- Create DBPerformance table
CREATE TABLE IF NOT EXISTS netbox_kpi.DBPerformance (
    TestId String,
    TestedTable String,
    Clients Int32,
    Threads Int32,
    Transactions Int32,
    Tps Float32,
    Latency Float32,
    AvgMemUsage Float32,
    FOREIGN KEY (TestId) REFERENCES TestCases(Id)
) ENGINE = MergeTree()
ORDER BY TestId;

-- Create VMPerformance table
CREATE TABLE IF NOT EXISTS netbox_kpi.HttpPerformance (
    TestId String,
    Concurrency Int32,
    Sec Int32,
    Path String,
    Txn Int32,
    TxnRate Float32,
    TxnSucc Int32,
    TxnFail Int32,
    FOREIGN KEY (TestId) REFERENCES TestCases(Id)
) ENGINE = MergeTree()
ORDER BY TestId;

-- Create CpuMemory table
CREATE TABLE IF NOT EXISTS netbox_kpi.VMCpuMemory (
    TestId String,
    Concurrency Int32,
    Sec Int32,
    Path String,
    Cpu Float32,
    Memory Int32,
    FOREIGN KEY (TestId) REFERENCES TestCases(Id)
) ENGINE = MergeTree()
ORDER BY TestId;

-- Create DBOpenSessions table
CREATE TABLE IF NOT EXISTS netbox_kpi.DBOpenSessions (
    TestId String,
    Concurrency Int32,
    Sec Int32,
    Path String,
    ActiveConnections Int32,
    FOREIGN KEY (TestId) REFERENCES TestCases(Id)
) ENGINE = MergeTree()
ORDER BY TestId;


-- +goose Down

-- drop tables
DROP TABLE IF EXISTS netbox_kpi.TestCases;
DROP TABLE IF EXISTS netbox_kpi.DBPerformance;
DROP TABLE IF EXISTS netbox_kpi.HttpPerformance;
DROP TABLE IF EXISTS netbox_kpi.VMCpuMemory;
DROP TABLE IF EXISTS netbox_kpi.DBOpenSessions;
