-- +goose NO TRANSACTION
-- +goose Up
CREATE DATABASE IF NOT EXISTS netbox_kpi;

-- +goose Down
DROP DATABASE IF EXISTS netbox_kpi;
