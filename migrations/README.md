
### Schema migration, with Goose tool

[https://github.com/pressly/goose](https://github.com/pressly/goose)

### Install stand-alone

Execute `./install.sh` file with your regular user in this folder. It will save the file on `~/.local/bin/goose`.

Or just download from [https://github.com/pressly/goose/releases](https://github.com/pressly/goose/releases),  
into your `~/.local/bin/` folder and execute as any other program with `goose --help`.


### Clickhouse variables set-up.

Example.
```
export \
GOOSE_DRIVER=clickhouse \
GOOSE_DBSTRING="tcp://clickuser:password1@localhost:9000/clickdb" \
GOOSE_MIGRATION_DIR="./"
```

### Local testing

To start local Clickhouse instance for testing  
[https://registry.hub.docker.com/r/clickhouse/clickhouse-server](https://registry.hub.docker.com/r/clickhouse/clickhouse-server)
```
docker run --rm -d -p 8123:8123 -p 9000:9000 --name some_clickhouse --ulimit nofile=262144:262144 clickhouse/clickhouse-server
```

To login, in case of need.
```
docker exec -it some_clickhouse clickhouse-client
```

Set up your variables.
```
export \
GOOSE_DRIVER=clickhouse \
GOOSE_DBSTRING="tcp://localhost:9000" \
GOOSE_MIGRATION_DIR="./"
```

To connect to remote, example:
```
export GOOSE_DBSTRING=tcp://netbox:${CLICKHOUSE_PASSWORD}@${CLICKHOUSE_HOST}:9000/goose"
```

Some commands to run.
```
goose status
goose up-by-one
goose down
```
