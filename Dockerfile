FROM netboxcommunity/netbox:v3.6.9-2.7.0

USER root
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        gcc \
        libpq-dev \
        python3-dev \
        python3-venv \
        git \
        && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /opt/netbox/plugins
COPY requirements.txt /opt/netbox/plugins
COPY netbox_configuration/plugins.py /etc/netbox/config/plugins.py

RUN pip install -r /opt/netbox/plugins/requirements.txt

RUN rm -rf /opt/netbox/plugins
