--
-- PostgreSQL Mappings dump
--
COPY public.netbox_rps_plugin_mapping (
	created,
	last_updated,
	custom_field_data,
	source,
	target,
	authentication,
	testingpage,
	webdav,
	"Comment",
	gzip_proxied,
	keepalive_requests,
	keepalive_timeout,
	proxy_cache,
	proxy_read_timeout,
	client_max_body_size,
	sorry_page,
	extra_protocols)
FROM stdin DELIMITER E'\t';
