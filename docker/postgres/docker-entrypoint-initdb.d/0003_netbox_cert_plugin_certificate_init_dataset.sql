--
-- PostgreSQL database dump
--
-- Data for Name: netbox_cert_plugin_certificate; Type: TABLE DATA; Schema: public; Owner: netbox
--
COPY public.netbox_cert_plugin_certificate (
	created,
	last_updated,
	custom_field_data,
	cn,
	ca,
	expiration_time,
	cert_created_at,
	cert_expired_at,
	alt_name,
	state,
	content,
	vault_url)
FROM stdin DELIMITER E'\t';
