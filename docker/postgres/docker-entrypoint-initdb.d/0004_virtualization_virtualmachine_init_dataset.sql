--
-- PostgreSQL database dump
--
-- create a clusterype record
insert into public.virtualization_clustertype (custom_field_data, name, slug, description) values ('{}', 'hey2_type', 'hey2_type', '');
-- create a cluster record
insert into public.virtualization_cluster (custom_field_data, name, comments, type_id, status, description) values ('{}', 'hey2_cluster', '', 1, 'Active', '');
--
-- Data for Name: virtualization_virtualmachine; Type: TABLE DATA; Schema: public; Owner: netbox
--
COPY public.virtualization_virtualmachine (
	created,
	last_updated,
	custom_field_data,
	local_context_data,
	name,
	status,
	vcpus,
	memory,
	disk,
	comments,
	cluster_id,
	platform_id,
	primary_ip4_id,
	primary_ip6_id,
	role_id,
	tenant_id,
	_name,
	site_id,
	device_id,
	description,
	interface_count,
	config_template_id)
FROM stdin DELIMITER E'\t';
