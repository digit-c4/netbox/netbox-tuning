# Testing Environment description

A Tests Suite is run in an testing environment. Describing and logging this
testing environment will allow us to track the evolution of our various results
over time or through configuration changes.

## Properties

* Name: required (string), the name of the testing environment (max length: 32
  characters). Must be unique. (eg: `Netbox latest`)
* [Netbox distribution](https://code.europa.eu/digit-c4/netbox-plugins) version:
  required (string), the version of the distribution we use for the tests suite
  (eg: `latest`).
* Netbox version: required (string), the version of Netbox we use for the Test
  Suite (eg: `3.7.4`).
* PostgreSQL version: required (string), the version of PostgreSQL we use for
  the Test Suite (eg: `16.2`).
    - rowsDump: required (integer), number of preloaded rows on several key tables.
* PostgreSQL settings:
    - max_connections: required (integer), the maximum number of concurrent
      connections to the database server (eg: `100`).
    - shared_buffers: required (integer), the amount of memory (in MB) the
      database server uses for shared memory buffers (eg: `512`).
    - work_mem: required (unsigned integer), the amount of memory (in KB) per connection.
* Redis version: required, the version of PostgreSQL we use for the Test Suite
  (eg: `7.2.4`).
* Machine description:
    - CPU: required (integer), the number of CPU that machine include (eg: `4`).
    - RAM: required (integer), the amount of RAM (in MB) the machine include
      (eg: `2048`).
* Gitlab test case:
    - TestContext: nmae of branch where has been executed.
