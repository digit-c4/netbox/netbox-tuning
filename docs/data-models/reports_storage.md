# Reporting Storage Datamodel

The purpose of this documentation is to describe how the Reporting Storage Datamodel is.

## Introduction
We are defining a way of storing the test results of the performance tests in ClickHouse. For that reason, we are defining in this document the Data Model for Test Environments, Test Cases and Test Results.

## Entities
The entities we have in our ER Diagram consists on the follwoing ones:

* Testing Environment
* Test Case
* Test Result

## Data model


```mermaid
erDiagram
	%%%%%%%%%%%%%%%%%
	%% Relations
	%%%%%%%%%%%%%%%%%
	dim_netbox_settings 	||--||	dim_testing_environment : link
	dim_environment_settings ||--||	dim_testing_environment : link
	dim_postgresql_settings ||--||	dim_testing_environment : link

	dim_testing_environment |o--|| txt_test_case : link

	txt_test_case }|--|| dim_test_input : link
	txt_test_case ||--o{ txt_test_results : link
	txt_test_case }|--|| dim_test_group : link

	txt_test_results ||--|{ dim_clickhouse_storage : link

	%%%%%%%%%%%%%%%%%%
	%% Tables
	%%%%%%%%%%%%%%%%%%
	dim_netbox_settings {
	netbox_settings_id	UInt32	PK
	netbox_distro_vesion	UInt32
	netbox_sql_version	UInt32
	}

	dim_testing_environment {
	testing_environment_id	UInt32 PK
	name			varchar(50)
	netbox_settings_id	UInt32	FK
	environment_settings_id	UInt32	FK
	postgresql_settings_id	UInt32	FK
	}

	txt_test_case {
	test_case_id		UInt32	PK
	testing_environment_id	UInt32	FK
	test_input_id		UInt32	FK
	test_group_id		UInt32	FK
	}

	txt_test_results {
	test_results_id		UInt32	PK
	test_case_id		UInt32	FK
	test_result_value	UInt32
	}

	dim_clickhouse_storage {
	clichouse_storage_id	UInt32	PK
	storage_location	varchar(50)
	test_result_id		UInt32	FK
	}

	dim_test_group {
	test_group_id		UInt32	PK
	test_group_name		varchar(50)
	}

	dim_test_input {
	test_input_id		UInt32	PK
	test_input_name		varchar(50)
	test_input_value	UInt32
	}

	dim_environment_settings {
	environment_settings_id	UInt32	PK
	cpu			UInt32
	ram			UInt32
	redis_version		UInt32
	postgresql_version	UInt32
	}

	dim_postgresql_settings {
	postgresql_settings_id	UInt32	PK
	max_connections		UInt32
	shared_buffers		UInt32
	}

```

## Implementation on ClickHouse

```mermaid
erDiagram
	%%%%%%%%%%%%%%%%%
	%% Relations
	%%%%%%%%%%%%%%%%%
	VMCpuMemory |o--|| TestCases : produce
	DBOpenSessions |o--|| TestCases : produce
	TestCases ||--o| DBPerformance : produce
	TestCases ||--o| HttpPerformance : produce

	%%%%%%%%%%%%%%%%%%
	%% Tables
	%%%%%%%%%%%%%%%%%%
	TestCases{
	Id		String PK
	TestDate	datetime
	NetboxDistributionVersion	String
	Cpu		String
	Ram		Int32
	RedisVersion	String
	PostgresqlVersion	String
	PostgresMaxConnections	Int32
	PostgresSharedBuffers	Int32
	PostgresWorkMem	UInt64
	TestContext	String
	RowsDump	UInt32
	}

	DBPerformance {
	TestId		String FK "Ref. TestCases(Id)"
	TestedTable	String
	Clients		Int32
	Threads		Int32
	Transactions	Int32
	Tps		Float32
	Latency		Float32
	AvgMemUsage	Float32
	}

	HttpPerformance {
	TestId		String FK "Ref. TestCases(Id)"
	Concurrency	Int32
	Sec		Int32
	Path		String
	Txn		Int32
	TxnRate		Float32
	TxnSucc		Int32
	TxnFail		Int32
	}

	VMCpuMemory {
	TestId		String FK "Ref. TestCases(Id)"
	Concurrency	Int32
	Sec		Int32
	Path		String
	Cpu		Float32
	Memory		Int32
	}

	DBOpenSessions {
	TestId		String FK "Ref. TestCases(Id)"
	Concurrency	Int32
	Sec		Int32
	Path		String
	ActiveConnections	Int32
	}
```

