# DB Performance report requirements

## Performance by Table

Below is a description of the test reports. All tests reports depend of a
[Testing Environment](./testing_environment.md).

The tests reports' objectives are to be able to determine a capacity limit in
order to keep the service alive and to provide enough performance for clients.

### Test report

For each tests for a SQL query, we provide Transaction per second (t/s), latency
(l) in ms, average memory usage (m) in MB by number of concurrent database clients (c):

* x-axis (abs.): c
* y-axis (ord.): t/s, l, m

Example `ex1` of dataset:

Time of the test: 2024-04-03T18:06:00

| c | t/s | l | m | t/s/c |
| --| ----| - | - | ----- |
| 1 | 619.5 | 1.573 | 40.8 | 619.5 |
| 2 | 1281.6 | 1.499 | 17.13 | 640.8 |
| 5 | 1683.1 | 2.614 | 27.01 | 841.6 |
| 10 | 1533.8 | 5.398 | 37.42 | 153.4 |

Where t/s/c is the request per second per client found during the test of a SQL Query. This provide a way to compare performance between the number of concurrent clients.
