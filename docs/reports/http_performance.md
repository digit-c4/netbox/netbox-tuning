# HTTP Performance report requirements

## Performance by number of concurrent clients

Below is a description of the test reports. All tests reports depend of a [Testing Environment](./testing_environment.md).

The tests reports' objectives are to be able to determine a rate limit in order to keep the service alive and to be fair with clients.

### Test report

For each tests on a [URL path](https://en.wikipedia.org/wiki/URL#path), we provide Request per second (r/s), number of failed
transaction (f) by Number of concurrent clients (c):

* x-axis (abs.): c
* y-axis (ord.): r/s, f

Example `ex1` of dataset:

Time of the test: 2024-04-03T18:06:00

| c | r/s | f | r/s/c |
| --| ----| - | ----- |
| 1 | 599 | 0 | 599 |
| 5 | 2598 | 0 | 519 |
| 10 | 1706 | 0 | 170 |
| 15 | 2665 | 0 | 177 |
| 30 | 3143 | 0 | 104 |
| 60 | 2297 | 15 | 38 |

Where r/s/c is the request per second per client found during the test of a URL
path. This provide a way to compare performance between the number of concurrent clients.

### Following test reports

#### a) Following tests reports with Slope request per second per concurrent clients (r/s/c) by time of the tests (t) over 6 month:

* x-axis (abs.): t
* y-axis (ord.): Slope r/s/c

where Slope r/s/c is `(r/s_c_max - r/s_c_min / (c_max - c_min)`:

* `r/s_c_max` is the value of r/s for the maximum of c
* `r/s_c_min` is the value of r/s for the minimum of c
* `c_max` is the value of c for the maximum of c
* `c_min` is the value of c for the minimum of c

Example `ex2` of dataset (from `ex1`):

| Slop r/s/c | t |
| ---------- | - |
| 28 | 2024-04-03T18:06:00 |
| … | … |

#### b) Following tests reports with request per second per maximum concurrent clients (r/s/c_max) by time of the tests (t) over 6 month:

Maximum concurrent clients is the number of concurrent clients above which is no
longer possible to acquire results.

* x-axis (abs.): t
* y-axis (ord.): r/s/c_max

Example `ex3` of dataset (from `ex1`):

| r/s/c_max | t |
| --------- | - |
| 38 | 2024-04-03T18:06:00 |
| … | … |
