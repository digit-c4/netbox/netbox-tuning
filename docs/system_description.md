# System Description

## Overview

The system context diagram is shown below. This diagram shows the actors and their interaction with the various systems involved.

![System Context Diagram](./system_context.png)

* Tester: Person or active component that are able to:
  - Define and trigger the execution of Tests Cases
  - Define and deploy Testing Environments
  - From the execution of Tests Cases, store Tests Reports in the Reporting Storage
  - Define and view Reports with the Reporting Viewer
* Tests Cases: Set of tests scenario to be executed in the context of
  measuring the performance and the capacity of a Testing Environment
* Testing Environment Deployer: Service that deploy Testing Environments given
  description
* Test Report Storer: Store Test Report in the Reporting Storage
* Testing Environment: Fully operational Netbox environment deployed according
  definitions provided by Tester.
* Reporting Storage: Database that contain all Tests Reports' Results collected
  from Testing Environments.
* Reporting Viewer: Component where Tester can define and view Reports in order
  to extract KPI in the context of the performance and the capacity of a Testing Environment.

### Sequence Diagram

The purpose of the sequence diagram is to show the order of interaction between the various system components.

![Sequence Diagram](./overview_sequence_diagram.png)

1. The Tester send Environment Definition to Testing Environment Deployer
2. The Testing Environment Deployer from definition deploy / create the Testing
   Environment
3. With the confirmation that the Testing Environment is deployed, the Tester
   trigger the execution of the Tests Cases on the Testing Environment freshly
   created
4. The Tester collect all Results from Test Cases execution
5. From Tests Cases execution Results the Tester push to the Test Reports Storer
   a compiled Tests Reports that will be stored to the Reporting Storage
6. Asynchronously, Tester cans define and consult the Testing Report and KPI
   compiled on the Reporting Viewer.

## Technical solution consider for implemantation

- Testing evironment has been done with "Gitlab CI" and "Ansible" help.
- For Test cases, we use "Ansible" tool, we have some standard values, and some of them can be manually modified using the "Gitlab CI forms".
- Test report storer. Organized and stored on a [Clickhouse DB](https://clickhouse.com/), OLAP Open-source solution and proper SQL syntax.
- Reporting Viewer. We are using ["Preset"](https://manage.app.preset.io/login/) tool to create related reports. 


## What could be next

- Report follow-up
- Able to compare test cases
- Start to analyse data
- Fix rate limit
- Add more data following business requirements
