
# Script "db_open_sessions.sh".

Script to recollect number of open sessions from Postgres.

**Help command.**
```
./db_open_sessions.sh -h
```

**To run command, example.**
```
./db_open_sessions.sh -H <postgres_container_id> -s <seconds_to_sleep> -r <repeat_number_of_times> -d /destination/folder/to/save/
```

##### CSV Values

Field			| Description
---			| ---
Concurrency		| Concurrent value within "siege" command.
Sec			| Seconds within "siege" command.
Path			| Path within "siege" command.
active_connections	| number of active connections with postgres
