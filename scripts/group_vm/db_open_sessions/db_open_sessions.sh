#!/bin/bash
#
#
# To execute this script
# ./db_open_sessions.sh -h
# ./db_open_sessions.sh -H <postgres_container_id> -s <sleep_time> -r <repeat_number_of_times>
#
#

# Defaults
SLEEP="5"
REITERATE="40"
HOST="localhost"
PORT=5432
DB="netbox"
USER="netbox"
PASS="J5brHrAXFLQSif0K"
# working directory
DIR2SAVE="${0%/*}/"
# verboxity
VERB=1

# Query to execute
QUERY="SELECT COUNT(*) FROM pg_stat_activity;"

# Define the help method
help() {
  echo "Usage: $(basename $0) [-h] [-s SEC]"
  echo "Options:"
  echo "  -h,	Display this help message"
  echo "  -s,	Set Seconds to sleep per loop (default: ${SLEEP})"
  echo "  -r,	Set amount of Repetitions, loop cicles (default: ${REITERATE})"
  echo "  -H,	Host (default: ${HOST})"
  echo "  -p,	Port (default: ${PORT})"
  echo "  -D,	Database (default: ${DB})"
  echo "  -U,	User (default: ${USER})"
  echo "  -P,	Password (default: ${PASS})"
  echo "  -x,	Http Siege test case config that is being executed. Usage: -x <"CONCURRENCY,SIEGE_SEC,ENDPOINT"> default("${HTTPCONFIG}")"
  echo "  -v,	Verbosity (default: ${VERB})"
  echo "  -d,	Full path Directory to save files (default: ${DIR2SAVE})"
  exit 0
}

# Parse command-line options
while getopts ":hs:r:H:p:d:U:P:x:v:d:" opt; do
  case $opt in
    h) help ;;
    s) SLEEP=$OPTARG ;;
    r) REITERATE=$OPTARG ;;
    H) HOST=$OPTARG ;;
    p) PORT=$OPTARG ;;
    D) DB=$OPTARG ;;
    U) USER=$OPTARG ;;
    P) PASS=$OPTARG ;;
    x) HTTPCONFIG=$OPTARG ;;
    v) VERB=$OPTARG ;;
    d) DIR2SAVE=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done
if [[ ${DIR2SAVE: -1} != '/' ]]
  then echo "Check Dir Path: $DIR2SAVE"; exit 1
fi

# Print variables
if [[ $VERB == 1 ]] ; then
	echo "Query to execute: ${QUERY}"
	echo "Sleep: ${SLEEP}"
	echo "Reiterate: ${REITERATE}"
	echo "Host: ${HOST}"
	echo "Port: ${PORT}"
	echo "Database: ${DB}"
	echo "User: ${USER}"
	echo "Pass: ${PASS}"
	echo "http_config: ${HTTPCONFIG}"
	echo "Save to directory: ${DIR2SAVE}" ; sleep 2
fi


# create CSV file, if not there.
if test ! -f "${DIR2SAVE}db_open_sessions.csv" ; then
	mkdir -p $DIR2SAVE
	echo "Concurrency,Sec,Path,active_connections" > ${DIR2SAVE}db_open_sessions.csv
fi


# loop
for (( i=0; i<REITERATE; i++ ))
do
	# log concurrency, sec and path to CSV file
	(echo "${HTTPCONFIG}" | tr '\n' ','; \
	# Get db_open_sessions from container
	echo $(docker exec $HOST /bin/bash -c "PGPASSWORD=${PASS} psql -U $USER -d $DB -h localhost -p ${PORT} -c '$QUERY'" | sed -n 3p | tr -d '[:space:]' ) \
	# save to CSV file
	) >> ${DIR2SAVE}db_open_sessions.csv

	# sleep
	sleep $SLEEP
done

echo "HTTP results saved to directory: ${DIR2SAVE}db_open_sessions.csv"

exit 0


