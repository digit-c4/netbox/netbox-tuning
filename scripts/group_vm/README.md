
## One Ring to rule them all

Script `ring.sh`, will execute other scripts in parallel.  
More Info about other scripts inside of each folder.

**Requirements Debian/Ubuntu**
```
 # on netbox container
apt-get install siege
 # on postgres container, it might already be installed by default.
apt-get install postgresql-contrib
```

**Help command.**
```
./ring.sh -h
```

**To run command, example.**
```
./ring.sh -t <netbox_token> -H <netbox_container_id> -c <postgres_container_id> -d /path/to/folder/
./ring.sh -t e980f5e943aa56af777a08b5477d6690f13f1c97 -H 798d8aa1ef9b -c ae54e290840e -d /home/debian/netbox/netbox-tuning/data/output/ 
```






