#!/bin/bash
#
#
# To execute this script
# ./ring.sh -h
# ./ring.sh -t <token> -H <netbox_container_id> -c <psg_container_id> -d /path/to/folder/
#
#

# Default values
TOKEN=""
SEC="5"
NETBOX_CONTAINER_ID="localhost"
PSG_CONTAINER_ID=""
HTTPCONFIG=",,"
#DIR2SAVE=$HOME/.kpi/
# working directory
DIR2SAVE="${0%/*}/"
WORKDIR=$DIR2SAVE

# Endpoints
ENDPOINT_ARRAY=( \
"/api/circuits/" \
"/api/core/" \
"/api/dcim/" \
"/api/extras/" \
"/api/ipam/" \
"/api/virtualization/virtual-machines/" \
"/api/wireless/" \
"/api/plugins/rps/mapping/" \
"/api/plugins/cert/certificate/" \
)
CONCURRENT_ARRAY=( \
"1" \
"2" \
"5" \
"10" \
"15" \
"30" \
)

# Define the help method
help() {
  echo "Usage: $(basename $0) [-h] [-s SEC]"
  echo "Options:"
  echo "  -h,	Display this help message"
  echo "  -t,	Set Netbox Token"
  echo "  -s,	Set Seconds for 'siege' comand, default(${SEC})"
  echo "  -H,	Netbox container id, default(${NETBOX_CONTAINER_ID})"
  echo "  -c,	Postgres container id, default(${PSG_CONTAINER_ID})"
  echo "  -d,	Full path Directory to save files, default(${DIR2SAVE})"
  exit 0
}

# Parse command-line options
while getopts ":ht:H:s:c:d:" opt; do
  case $opt in
    h) help ;;
    t) TOKEN=$OPTARG ;;
    H) NETBOX_CONTAINER_ID=$OPTARG ;;
    s) SEC=$OPTARG ;;
    c) PSG_CONTAINER_ID=$OPTARG ;;
    d) DIR2SAVE=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done
if [[ $1 == '' ]]; then help ; fi
if [[ ${DIR2SAVE: -1} != '/' ]]
  then echo "Check Dir Path: $DIR2SAVE"; exit 1
fi

# Make sure sec is not shorter that 3 secs.
if [[ 3 > $SEC ]] ; then SEC=3 ; fi
# Run just once per loop
REITERATE=1

# loop
for endpoint in "${ENDPOINT_ARRAY[@]}"
do
	for concurrent in "${CONCURRENT_ARRAY[@]}"
	do
		# run http siege.
		${WORKDIR}http/http.sh -t $TOKEN -H $NETBOX_CONTAINER_ID -s $SEC -e $endpoint -x $concurrent -d $DIR2SAVE -v 0 &
		# register pid
		http_pid=$!
		# trap it!
		trap "kill $http_pid 2> /dev/null" EXIT

		# wait close to the end before collection others
		sleep $(( SEC - 2 ))

		# Run cpu script once, quiet mode.
		${WORKDIR}cpu/cpu.sh -H $NETBOX_CONTAINER_ID -s 0 -r $REITERATE -x "${concurrent},${SEC},${endpoint}" -d $DIR2SAVE -v 0

		# Run open_sessions script once, quiet mode.
		${WORKDIR}db_open_sessions/db_open_sessions.sh -H $PSG_CONTAINER_ID -s 0 -r $REITERATE -x "${concurrent},${SEC},${endpoint}" -d $DIR2SAVE -v 0

		# wait two seconds for siege to finish
		sleep 2

		# if siege is still running give extra secs
		if kill -0 $http_pid 2> /dev/null; then
			echo "Siege is taking some extra time."
			sleep 2
		fi
		# if siege is still running...kill!
		if kill -0 $http_pid 2> /dev/null; then
			echo "Killing mode!"
			kill -9 $http_pid
			sleep 2
		fi
		# Disable the trap on a normal exit.
		trap - EXIT
	done
done

exit 0
