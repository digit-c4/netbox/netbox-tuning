######################
#
# Description:
#  This script will help us to get a temporal token from Netbox using API.
#
# How to execute
# ./netbox_token <user> <pass> <host:port>
#
# $1, username
# $2, password
# $3, host:port
# $4, token_timeout in minutes.
#
# note to extract date from ansible => {{ '%Y-%m-%dT%H:%M:%SZ' | strftime(ansible_date_time.epoch | int + 300) }}
#
######################
curl --header "Content-Type: application/json"   --request POST \
 --data '{"username":"'${1}'","password":"'${2}'","write_enable":true,"expires":"'$(date +%Y-%m-%dT%H:%M:%SZ -d "$DATE + ${4} min")'"}' \
 http://${3}/api/users/tokens/provision/
