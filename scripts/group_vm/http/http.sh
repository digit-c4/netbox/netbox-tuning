#!/bin/bash
#
#
# To execute this script
# ./script -h
# ./script -t <token> -p <port> -H <netbox_container_id>
# ./http/http.sh -t d4f51847e98add9ca7536ef875dbe96084345ff6 -H 798d8aa1ef9b -d /home/debian/netbox/netbox-tuning/data/output/ -x "1 3 5" -e "/api/dcim/ /api/plugins/rps/mapping/"
#
#


# Default values
TOKEN=""
PORT="8080"
HOST="localhost"
SEC="2"
DIR2SAVE=$HOME/.kpi/
SIEGE_CONFIG_PATH=""
# verboxity
VERB=1

# Define the help method
help() {
  echo "Usage: $(basename $0) [-h] [-t TOKEN] [-p PORT] [-H HOST] [-s SEC]"
  echo "Options:"
  echo "  -h,	Display this help message"
  echo "  -t,	Set Token"
  echo "  -p,	Set Port, default(${PORT})"
  echo "  -H,	Set Host, default(${HOST})"
  echo "  -s,	Set Seconds per comand, default(${SEC})"
  echo "  -e,	Endpoint for siege"
  echo "  -x,	Concurrency for siege"
  echo "  -v,	Verbosity (default: ${VERB})"
  echo "  -d,	Full path Directory to save files, default(${DIR2SAVE})"
  exit 0
}

# Parse command-line options
while getopts ":ht:p:H:s:e:x:v:d:" opt; do
  case $opt in
    h) help ;;
    t) TOKEN=$OPTARG ;;
    p) PORT=$OPTARG ;;
    H) HOST=$OPTARG ;;
    s) SEC=$OPTARG ;;
    e) ENDPOINT_ARRAY=( $OPTARG ) ;;
    x) CONCURRENT_ARRAY=( $OPTARG ) ;;
    v) VERB=$OPTARG ;;
    d) DIR2SAVE=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done
if [[ $1 == '' ]]; then help ; fi
if [[ ${DIR2SAVE: -1} != '/' ]]
  then echo "Check Dir Path: $DIR2SAVE"; exit 1
fi
# defaults
if [[ $ENDPOINT_ARRAY == '' ]]; then
	# Endpoints
	ENDPOINT_ARRAY=( \
	"/api/dcim/" \
	"/api/plugins/rps/mapping/" \
	)
fi
if [[ $CONCURRENT_ARRAY == '' ]]; then
	CONCURRENT_ARRAY=( \
	"1" \
	"2" \
	"5" \
	)
fi

# Print variables
if [[ $VERB == 1 ]] ; then
	echo "Siege Sec: ${SEC}"
	echo "Saving to directory: ${DIR2SAVE}"
fi

# create CSV file, if not there.
if test ! -f "${DIR2SAVE}http.csv" ; then
	mkdir -p $DIR2SAVE
	echo "Concurrency,Sec,Path,Txn,TxnRate,TxnSucc,TxnFail" > ${DIR2SAVE}http.csv
fi

# log `new line`
echo " " >> ${DIR2SAVE}http.log

# loop
for endpoint in "${ENDPOINT_ARRAY[@]}"
do
	# Header to use with siege request
	if [[ ${endpoint:0:4} == "/api" ]] ; then
		HEADER="Authorization: Token $TOKEN"
	else
		HEADER="Cookie: csrftoken=$TOKEN"
	fi

	# concurrent clients to use.
	for concurrent in "${CONCURRENT_ARRAY[@]}"
	do
		# log date
		echo $(date '+%Y-%m-%d %H:%M:%S %z') | tr '\n' ',' >> ${DIR2SAVE}http.log

		# print command to execute
		echo "siege -b -c$concurrent -t${SEC}s --header=\"$HEADER\" http://localhost:$PORT$endpoint"
		# execute siege commnad
		docker exec -u ubuntu $HOST /bin/bash -c "siege -b -c$concurrent -t${SEC}s --no-parser --no-follow \
			--header='$HEADER' http://localhost:$PORT$endpoint \
			--rc=$SIEGE_CONFIG_PATH " \
			|sed $'s/[^[:print:]\t]//g' \
			|grep -e "transactions" -e "tion_rate" |tr '\t' ' ' |sed 's/{//' |tr '\n' ' ' |tr -s ' ' 1>> ${DIR2SAVE}http.log

		# log some variables
		echo "concurrent: $concurrent, seconds: ${SEC}, endpoint: $endpoint" >> ${DIR2SAVE}http.log

		# append to csv
		cat ${DIR2SAVE}http.log | sed -n '$p' |awk '$16~/endpoint/{print $13$15$17","$5$7$9$11}' >> ${DIR2SAVE}http.csv

	done
done

echo "HTTP results saved to directory: ${DIR2SAVE}http.csv"

exit 0


