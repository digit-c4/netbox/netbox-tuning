
# Script "http.sh".

Script using siege to test netbox HTTP URLs.

**Requirements.**
```
sudo apt-get install siege
```

**Help command.**
```
./http.sh -h
```

**To run command, example.**
```
./http.sh -t <token> -p <port> -H <netbox_container_id>
./http/http.sh -t d4f51847e98add9ca7536ef875dbe96084345ff6 -H 798d8aa1ef9b -d /home/debian/netbox/netbox-tuning/data/output/ -x "1 3 5" -e "/api/dcim/"
```

##### Output Path

`ls ~/.kpi/`  
CSV files and log files are being sent there, by default.


##### CSV Values

Field		| Description
---		| ---
Concurrent	| Concurrent value within "siege" command.
Sec		| Seconds within "siege" command.
Path		| Path within "siege" command.
Txn		| Transactions
TxnRate		| Transaction_rate
TxnSucc		| Transactions_success
TxnFail		| Transactions_fail

