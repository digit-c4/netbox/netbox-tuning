
# Script "cpu.sh".

Script to recollect some simple CPU && Memory values.

**Help command.**
```
./cpu.sh -h
```

**To run command, example.**
```
./cpu.sh -H <hostname_or_netbox_container_id> -s <seconds_to_sleep> -r <repeat_number_of_times> -d /destination/folder/to/save/
```

##### Output Path

`ls ~/.kpi/`  
CSV file is being sent there, by default.

##### CSV Values

Field		| Description
---		| ---
Concurrency	| Concurrent value within "siege" command.
Sec		| Seconds within "siege" command.
Path		| Path within "siege" command.
Cpu		| System load averages for the last minute
Memory		| Memory_in_use = (Total_memory - Available_memory)
