#!/bin/bash
#
#
# To execute this script
# ./cpu.sh -h
# ./cpu.sh -H <hostaname_or_docker_container_id> -s <sleep_time> -r <repeat_number_of_times>
#
#

# Default values
HOST="localhost"
SLEEP="5"
REITERATE="40"
DIR2SAVE=$HOME/.kpi/
HTTPCONFIG=",,"
# verboxity
VERB=1

# Define the help method
help() {
  echo "Usage: $(basename $0) [-h] [-s SEC]"
  echo "Options:"
  echo "  -h,	Display this help message"
  echo "  -H,	Host (default: ${HOST})"
  echo "  -s,	Set Seconds to sleep per loop, default(${SLEEP})"
  echo "  -r,	Set amount of Repetitions, loop cicles, default(${REITERATE})"
  echo "  -v,	Verbosity (default: ${VERB})"
  echo "  -x,	Http Siege test case config that is being executed. Usage: -x <"CONCURRENCY,SIEGE_SEC,ENDPOINT"> default("${HTTPCONFIG}")"
  echo "  -d,	Full path Directory to save files, default(${DIR2SAVE})"
  exit 0
}

# Parse command-line options
while getopts ":hH:s:r:v:x:d:" opt; do
  case $opt in
    h) help ;;
    H) HOST=$OPTARG ;;
    s) SLEEP=$OPTARG ;;
    r) REITERATE=$OPTARG ;;
    v) VERB=$OPTARG ;;
    x) HTTPCONFIG=$OPTARG ;;
    d) DIR2SAVE=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done
if [[ ${DIR2SAVE: -1} != '/' ]]
  then echo "Check Dir Path: $DIR2SAVE"; exit 1
fi

# Print variables
if [[ $VERB == 1 ]] ; then
	echo "Host: ${HOST}"
	echo "Sleep: ${SLEEP}"
	echo "Reiterate: ${REITERATE}"
	echo "Save to directory: ${DIR2SAVE}" ; sleep 2
fi


# create CSV file, if not there.
if test ! -f "${DIR2SAVE}cpu.csv" ; then
	mkdir -p $DIR2SAVE
	echo "Concurrency,Sec,Path,Cpu,Memory" > ${DIR2SAVE}cpu.csv
fi

# loop
for (( i=0; i<REITERATE; i++ ))
do
	# log concurrency, sec and path to CSV file
	(echo "${HTTPCONFIG}" | tr '\n' ','; \
	# get last minute CPU AVG value.
	docker exec $HOST /bin/bash -c "uptime" |awk '$0{print $(NF-2)}'|sed 's/.$//' |tr '\n' ','; \
	# get memory usage
	docker exec $HOST /bin/bash -c "free -m" |grep Mem|awk '$2{print $2-$7}' \
	# save info to CSV file
	) >> ${DIR2SAVE}cpu.csv

	# sleep
	sleep $SLEEP
done

echo "HTTP results saved to directory: ${DIR2SAVE}cpu.csv"

exit 0


