
# Script "db_performance_full_data.sh"

Script to collect DB Memory Usage and DB Query Performance.  
Edit default values inside script, in case of need.

### To Run

```
./db_performance_full_data.sh -h
./db_performance_full_data.sh -d /Path/to/folder/ -c <Postgres_container_name>
```


### Tests Tuning

It is possible to modify the following script variables in order to set different tests conditions

Variable	| Description
---			| ---
`tables`	| Tables for query execution
`query`		|  Query scenario (default: SELECT with filter & order)
`test_parameters`	|  Baseline test conditions. You can adjust the parameters passed to pgbench according to your testing requirements: https://www.postgresql.org/docs/current/pgbench.html


### Output CSV

Results CSV will be allocated under the script folder path.

Field			| Description
---				| ---
Table			| Netbox table against which the queries have been executed 
Clients			| Number of clients being tested 
Threads			| Number of threads being used during test
Transactions		| Number of transactions per client
Transactions per Second (TPS)	| Number of completed transactions processed by the database system in one second
Latency			|  Average(ms), Time it takes for a single query or transaction to be processed by the database system in milliseconds
Avg Memory Usage (MB)	|  Average memory usage by the data base container during the test

