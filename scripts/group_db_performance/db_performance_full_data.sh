#!/bin/bash

# working directory
DIR2SAVE="${0%/*}/"
# include config file
source ${DIR2SAVE}db_perf.conf

# Help module
help() {
  echo "Usage: $(basename $0) [-h]"
  echo "Options:"
  echo "  -d,	Full path Directory to save files (default: ${DIR2SAVE})"
  echo "  -c,	Postgres container name"
  exit 0
}

# Parse command-line options
while getopts ":hd:c:" opt; do
  case $opt in
    h) help ;;
    d) DIR2SAVE=$OPTARG ;;
    c) CONTAINER_NAME=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done

# create array with first value in.
test_parameters=("1 $DB_PERF_THREAD $DB_PERF_TRANSACTION")
# populate array with test parameters
for (( i=0; (i+1) < DB_PERF_AMOUNT_TESTS; i++ )); do
	# test: clients - jobs - transactions
	# first value, if 100 connections, then divide by 4 and give `25` `50` `75` and `100` values
	test_parameters+=("$(( DB_PERF_MAX_CONN / (DB_PERF_AMOUNT_TESTS-1) * (i+1) )) $DB_PERF_THREAD $DB_PERF_TRANSACTION")
done
# print values
printf "test_parameters array size is %d\n" "${#test_parameters[@]}"
printf "%s \n" "${test_parameters[@]}"

# Function to run pgbench
run_pgbench() {
    local c="$1"
    local j="$2"
    local t="$3"

    local postgres_ip=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$container_id")
    # Run pgbench
    docker exec -u root -ti "$container_id" pgbench -n -P 5 -U netbox -d netbox -h localhost -p 5432 -c "$c" -j "$j" -t "$t" -f /tmp/queries.sql

    # Get the process ID of the pgbench command
    local pgbench_pid="$!"
}

# Function to collect memory usage
collect_memory_usage() {
    local container_id="$1"
    local results_file="$2"
    local DIR2SAVE="$3"
    local memory_total=0
    local counter=0
    avg=0

    # Loop to collect memory usage
    while true; do
        local results_avg_file="${DIR2SAVE}memory_usage_avg_results.csv"
        local memory_read=$(docker stats --no-stream --format '{{.MemUsage}}' "$container_id" | awk  -FM '$0 {print $1}' )
        sum=$(echo "$memory_total $memory_read" | (awk '{ sum = $1 + $2; print sum }') ) 
        memory_total=$(echo "$memory_total $sum" | (awk '{ $1 = $2 ; print $1 }') ) 
        counter=$(echo "$counter" | (awk '{ sum = $1 + 1; print sum }') ) 
        avg=$(echo "$memory_total $counter" | (awk '{ sum = $1 / $2; print sum }') ) 
        echo "AVGMemoryUsage" > "$results_avg_file"
        echo "$avg" > "$results_avg_file"

        sleep 1
    done &
    # Get the process ID of the memory usage collection
    memory_pid=$!
}

# Main function
main() {
    # Initialize container
    local container_id=$(docker ps -q -f name=${CONTAINER_NAME})

    # Open CSV file to store results
    local results_file="${DIR2SAVE}memory_usage_results_agg.csv"
    echo "Table,Clients,Threads,Transactions,Transactions per Second (TPS),Latency,Avg Memory Usage (MB)" > "$results_file"
    docker exec -u root -ti "$container_id" sh -c "echo '' > /tmp/queries.sql"

    # Run tests
    for param in "${test_parameters[@]}"; do
        clients=$(echo "$param" | cut -d' ' -f1)
        threads=$(echo "$param" | cut -d' ' -f2)
        transactions=$(echo "$param" | cut -d' ' -f3)

        tables=('django_migrations' 'auth_user' 'django_admin_log' 'netbox_rps_plugin_mapping' 'virtualization_virtualmachine' 'netbox_cert_plugin_certificate')

        # Default stats reading
        local default_memory_read=$(docker stats --no-stream --format '{{.MemUsage}}' "$container_id" | awk  -FM '$0 {print $1}' ) 
        echo "$default_memory_read" >>"${DIR2SAVE}memory_usage_avg_results.csv"

        # Start memory usage collection
        collect_memory_usage "$container_id" "$results_file" "$DIR2SAVE"

        # Get the process ID of the memory usage collection
        for table in "${tables[@]}"; do
        query="SELECT * FROM ${table} order by id;"
        docker exec -u root -ti "$container_id" sh -c "echo -e '$query' >> /tmp/queries.sql"

        # Run pgbench
        result=$(run_pgbench "$clients" "$threads" "$transactions")

        #Extract TPS
        tps=$(echo "$result" | grep -oP 'tps = \K\d+\.\d+' | sed -n '1p')
        latency=$(echo "$result" | grep -oP 'latency average = \K\d+\.\d+')

        #Get Average memory data
        local csv_avg=$(awk -F "\"*,\"*" '{print $1}' ${DIR2SAVE}memory_usage_avg_results.csv)

        # Write data results to CSV
        echo "$table,$clients,$threads,$transactions,$tps,$latency,$csv_avg"  >> "$results_file"
        docker exec -u root -ti "$container_id" sh -c "echo '' > /tmp/queries.sql"

        done

        # Stop memory gathering process
        kill -9 "$memory_pid" &
        # Remove support File and Clean queries file
        rm -f "${DIR2SAVE}memory_usage_avg_results.csv"
        # Restart dockec container
        docker restart "$container_id" >&-
        #Wait 5 Seconds
        sleep 5
    done

    echo "DB performance results saved to directory: $results_file"

    # Clean up
    #docker rm -f "$container_id"
}
##########################################################
# Testing DB Memory Usage + DB Query Performance
##########################################################
# copy past data
cp -a "${DIR2SAVE}memory_usage_results_agg.csv" "${DIR2SAVE}bck_memory_usage_results_agg-$(date +"%m-%d-%y-%r").csv"

# Execute main function
main

