#!/bin/bash
#
#
# To execute this script
# ./cpu.sh -h
# ./cpu.sh -t <transactions> -d /path/to/folder/
#
#


# Default test conditions
CLIENT=10
JOBS=2
TXN=10000
# Other defaults
HOST="127.0.0.1"
PORT=5432
DB="netbox"
USER="netbox"
PASS="J5brHrAXFLQSif0K"
# working directory
DIR2SAVE="${0%/*}/"

# Define the help method
help() {
  echo "Usage: $(basename $0) [-h] [-s SEC]"
  echo "Options:"
  echo "  -h,	Display this help message"
  echo "  -c,	Clients, number of concurrent database clients (default: ${CLIENT})"
  echo "  -j,	Jobs, number of threads (default: ${JOBS})"
  echo "  -t,	Transactions, number of transactions each client runs (default: ${TXN})"
  echo "  -H,	Host (default: ${HOST})"
  echo "  -p,	Port (default: ${PORT})"
  echo "  -D,	Database (default: ${DB})"
  echo "  -U,	User (default: ${USER})"
  echo "  -P,	Password (default: ${PASS})"
  echo "  -d,	Full path Directory to save files (default: ${DIR2SAVE})"
  exit 0
}

# Parse command-line options
while getopts ":hc:j:t:H:p:D:U:P:d:" opt; do
  case $opt in
    h) help ;;
    c) CLIENT=$OPTARG ;;
    j) JOBS=$OPTARG ;;
    t) TXN=$OPTARG ;;
    H) HOST=$OPTARG ;;
    p) PORT=$OPTARG ;;
    D) DB=$OPTARG ;;
    U) USER=$OPTARG ;;
    P) PASS=$OPTARG ;;
    d) DIR2SAVE=$OPTARG ;;
    :) echo "Missing argument: -$OPTARG" >&2; exit 1 ;;
    \?) echo "Invalid option: -$OPTARG" >&2; exit 1 ;;
  esac
done
if [[ ${DIR2SAVE: -1} != '/' ]]
  then echo "Check Dir Path: $DIR2SAVE"; exit 1
fi

# Print variables
echo "Client: ${CLIENT}"
echo "Jobs: ${JOBS}"
echo "Txn: ${TXN}"
echo "Host: ${HOST}"
echo "Port: ${PORT}"
echo "Database: ${DB}"
echo "User: ${USER}"
echo "Pass: ${PASS}"
echo "Save to directory: ${DIR2SAVE}" ; sleep 2


# Output CSV file path
output_file="${DIR2SAVE}query_performance_c${CLIENT}j${JOBS}t${TXN}.csv"


# Define the tables for query execution
tables=('django_migrations' 'auth_user' 'django_admin_log' 'netbox_rps_plugin_mapping' 'virtualization_virtualmachine' 'netbox_cert_plugin_certificate')



# Function to execute query
execute_query() {
    # Write SQL query to file
    echo -e "$query" > /tmp/query.sql

    # Execute pgbench command
    command="PGPASSWORD=${PASS} pgbench -U $USER -d $DB -h $HOST -p $PORT -c $CLIENT -j $JOBS -t $TXN -f /tmp/query.sql"
    
    echo "$command"
    result=$(eval "$command")
    echo "$result"
}

# Main function
main() {
    # Write CSV header
    echo "Table,Latency Average (ms),Transactions per Second (TPS)" > "$output_file"

    # Iterate over tables and execute queries
    for table in "${tables[@]}"; do

        # Define the query scenario (SELECT with filter & order)
        query="SELECT * FROM ${table} order by id;"

        # Execute the query and measure performance
        result=$(execute_query $query)
        
        # Extract latency and TPS
        latency=$(echo "$result" | grep -oP 'latency average = \K\d+\.\d+')
        tps=$(echo "$result" | grep -oP 'tps = \K\d+\.\d+')

        # Write results to CSV
        echo "$table,$latency,$tps" >> "$output_file"
    done
}

# Execute main function
main
