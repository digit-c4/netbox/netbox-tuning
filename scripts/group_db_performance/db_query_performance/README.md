
# Script "database_query_performance.sh"

Script to collect database performance queries KPIs


### Run scripts

```
./database_query_performance.sh -h
./database_query_performance.sh -t <transaction_number> -d /Path/to/folder/
```


### Tests Tuning

It is possible to modify the following script variables in order to set different tests conditions

Variable	| Description
---	| ---
`tables`	| Tables for query execution
`queries`	|  Query scenarios (default: SELECT with filter & order)
`testsConditions`	|  Baseline test conditions. You can adjust the parameters passed to pgbench according to your testing requirements: https://www.postgresql.org/docs/current/pgbench.html


### Output CSV

Results CSV will be allocated under the script folder path.

Field	| Description
---	| ---
Table	| Netbox table against which the queries have been executed 
Latency Average (ms)	|  Time it takes for a single query or transaction to be processed by the database system in milliseconds
Transactions per Second (TPS)	| Number of completed transactions processed by the database system in one second
