#!/bin/bash 
# Function to run pgbench
run_pgbench() {
    local c="$1"
    local j="$2"
    local t="$3"

    tables=('django_migrations' 'auth_user' 'django_admin_log' 'netbox_rps_plugin_mapping' 'virtualization_virtualmachine' 'netbox_cert_plugin_certificate')

    # Init pgbench
    #PGPASSWORD=J5brHrAXFLQSif0K pgbench -U netbox -d netbox -h 127.0.0.1 -p 5432 -i -q 

    #Create custom script to test
    for table in "${tables[@]}"; do
    query="SELECT * FROM ${table} order by id;"
    echo -e "$query" >> /tmp/queries.sql
    done

    # Run pgbench
    PGPASSWORD=J5brHrAXFLQSif0K pgbench -P 5 -U netbox -d netbox -h 127.0.0.1 -p 5432 -c "$c" -j "$j" -t "$t" -f /tmp/queries.sql


    # Get the process ID of the pgbench command
    local pgbench_pid="$!"

}

# Function to collect memory usage
collect_memory_usage() {
    local container_id="$1"
    local results_file="$2"
    local memory_total=0
    local counter=0
    local avg=0

    # Loop to collect memory usage
    while true; do
        local results_avg_file="memory_usage_avg_results.csv"
        local readings_log_file="readings.log"
        local memory_read=$(docker stats --no-stream --format '{{.MemUsage}}' "$container_id" | awk  -F. '$0 {print $1}' ) 
        echo $((sum= memory_total + memory_read))
        echo $((memory_total= sum))
        echo $((counter= counter + 1))
        echo $((avg= memory_total / counter))
  
        echo "AVGMemoryUsage" > "$results_avg_file"
        echo "$avg" > "$results_avg_file"

        sleep 1
    done &
    # Get the process ID of the memory usage collection
    local memory_pid="$!"
}

# Main function
main() {
    # Initialize container
    local container_id=$(docker ps -q -f name=tuning_postgres_1)

    # Define test parameters
    declare -a test_parameters=(
        #"1 1 1" #testing
        "1 1 10000"
        "2 2 10000"
        "5 4 10000"
        "10 4 10000"
        #"100 4 10000"
    )

    # Open CSV file to store results
    local results_file="memory_usage_results.csv"
    echo "Clients,Threads,Transactions,Transactions per Second (TPS),Latency,Avg Memory Usage (MB)" > "$results_file"
    echo "" > /tmp/queries.sql

    # Run tests
    for param in "${test_parameters[@]}"; do
        clients=$(echo "$param" | cut -d' ' -f1)
        threads=$(echo "$param" | cut -d' ' -f2)
        transactions=$(echo "$param" | cut -d' ' -f3)

        # Default stats reading
        local default_memory_read=$(docker stats --no-stream --format '{{.MemUsage}}' "$container_id" | awk  -F. '$0 {print $1}' ) 
        echo "$default_memory_read" >>"memory_usage_avg_results.csv"

        
        # Start memory usage collection
        collect_memory_usage "$container_id" "$results_file"
        
        # Get the process ID of the memory usage collection
        local memory_pid="$!"

        # Run pgbench
        result=$(run_pgbench "$clients" "$threads" "$transactions")

        #Extract TPS
        tps=$(echo "$result" | grep -oP 'tps = \K\d+\.\d+')
        latency=$(echo "$result" | grep -oP 'latency average = \K\d+\.\d+')

        local csv_avg=$(awk -F "\"*,\"*" '{print $1}' memory_usage_avg_results.csv)

        # Write memory usage results to CSV
        echo "$clients,$threads,$transactions,$tps,$latency,$csv_avg"  >> "$results_file"

        # Stop memory gathering process
        kill $memory_pid >&-

        # Remove support File and Clean queries file
        rm -f memory_usage_avg_results.csv
        echo "" > /tmp/queries.sql
        # Restart dockec cotainer
        docker restart "$container_id"
        #Wait 5 Seconds
        sleep 5
        
    done

    # Clean up
    #docker rm -f "$container_id"
}

# Execute main function
main

