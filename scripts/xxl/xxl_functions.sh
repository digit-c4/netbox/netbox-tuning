#!/bin/bash
#
#
# There is no random values on this script.
# All the values are create based on row numbers.
#

# create a specific date with `row number` as timestamp
timestamp_date() { echo $(date +%Y-%m-%dT%H:%M:%SZ -d @$1); }

# create a specific string with `row number`
func_str() { echo $1 |sha256sum |cut -c -15; }

# create a specific URL with `row number`
func_url() { echo "https://xxl-${1}-$(func_str $1).co.uk"; }
func_url_dest() { echo "https://xxl-dest-$(func_str $1).co.uk"; }

# create a specific comment with `row number`
func_comment() { echo "This is a long comment: $(func_str $1) $(func_str $1) $(func_str $1) $(func_str $1) $(func_str $1)"; }

# return AUTH method
func_auth() { NUMBER=$1; if (( NUMBER % 2 )); then echo Ecas; elif (( NUMBER % 3)); then echo Ldap; else echo None; fi ; }
