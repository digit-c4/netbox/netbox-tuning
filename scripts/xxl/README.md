### Scripts on XXL folder

The following scripts will create rows as output for specific Netbox tables.  
By default it is just output, it is not included anywhere.  
It is up to the user to redirect to specific target file.

- `http_mapping.sh`, will create rows for Postgres `public.netbox_rps_plugin_mapping` table.
- `cert_plugin.sh`, will create rows for Postgres `public.netbox_cert_plugin_certificate` table.
- `virt_vm.sh`, will create rows for Postgres `public.virtualization_virtualmachine` table.

### Others

- `xxl.conf` file to decide number of rows to create per table.
- `xxl_functions.sh` script is just a library with functions used by previous scripts.

