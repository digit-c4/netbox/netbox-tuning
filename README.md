# Netbox Performance & Capacity

## Requirements

- docker
- docker compose v2

## Contribute

Clone this project:

```
git clone git@code.europa.eu:digit-c4/netbox-tuning.git
```

##  Start Netbox Instance

env TAG=latest docker compose up

We use the [DIGIT-C4 Netbox
distribution](https://code.europa.eu/digit-c4/netbox-plugins/container_registry/90).
You can choose the version of the distribution with the environment variable
`TAG`.

It's possible to select a TAG by default with creating a `.env` file with this content:

```
TAG=latest
```

## How it's work?

You can start to guess by reading [the documentation](./docs/) or directly [tests scripts](./scripts/)
