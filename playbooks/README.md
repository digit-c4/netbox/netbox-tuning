# How to test playbooks manually


**To trigger manually, and target your own lab, example commands**
```
export CI_PIPELINE_ID="netbox-tuning"
export PG_SHARED_BUFFERS="1GB"
export PG_MAX_CONNECTIONS=100
export PG_WORK_MEM=4MB
export DEPLOY_TAG=latest
export DATASET_SIZE=1000
ansible-playbook -i <mylab.com>, <playbook_to_run>.yml -e "ansible_user=debian"
```


